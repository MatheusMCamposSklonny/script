/// <reference types="cypress"/>

const BASE_URL = 'https://dashboard.cypress.io';
const DASHBOARDS_IDS = ['yugn9u', '9uqp9u', 'z8532w', 'gw5dsn', 'nw5ez9', '739cjf', '8iwxge', '28dpvp', 'a2m828', 'm31w96', 'v63pk6', 'fu1rze', 'zcmhmo', 'dhw28e', '1o46ce'];

describe('Relatorio_Diario', () => {

    it('Acessar gerar unificação via cypress', () => {
        let tag = '1'
        let tag_2 = '0'
        let page = 1

        for (const id of DASHBOARDS_IDS) {
            const url = `https://dashboard.cypress.io/projects/${id}`;
            cy.visit(url, { timeout: 10000 })

            const newRow = {
                'Data': new Date().toLocaleString().split(' ')[0],
                'Run': '',
                'Link': '',
                'Pulados': '',
                'Ignorados': '',
                'Erros': '',
                'Passaram': '',
                'Total': '',
                'Project ID': ''
            }


            cy.get(':nth-child(' + tag + ') > .run-list-item > .run-list-item--attributes > .horizontal-list > :nth-child(11)', { timeout: 10000 }).then(num => {
                newRow['Run'] = num.text();
            });

            cy.get('.run-list-item:eq(' + tag_2 + ')', { timeout: 10000 }).invoke('attr', 'href')
                .then(href => {
                    newRow['Link'] = BASE_URL + href;
                });

            cy.get(':nth-child(' + tag + ') > .run-list-item > .run-list-item--results > [data-cy="run-stats"] >> [data-cy="link-skipped"] > [data-cy="total-skipped"]').then(Pulados => {
                newRow['Pulados'] = parseInt(Pulados.text());
            });

            cy.get(':nth-child(' + tag + ') > .run-list-item > .run-list-item--results > [data-cy="run-stats"] >> [data-cy="link-pending"] > [data-cy=total-pending]').then(Ignorados => {
                newRow['Ignorados'] = parseInt(Ignorados.text());
            });

            cy.get(':nth-child(' + tag + ') > .run-list-item > .run-list-item--results > [data-cy="run-stats"] >> [data-cy="link-passed"] > [data-cy=total-passed]').then(Passaram => {
                newRow['Passaram'] = parseInt(Passaram.text());
            });

            cy.get(':nth-child(' + tag + ') > .run-list-item > .run-list-item--results > [data-cy="run-stats"] >> [data-cy="link-failed"] > [data-cy=total-failed]').then(Erros => {
                newRow['Erros'] = parseInt(Erros.text());

                newRow['Total'] = +newRow['Pulados'] + +newRow['Ignorados'] + +newRow['Passaram'] + +newRow['Erros'];

                newRow['Project ID'] = DASHBOARDS_IDS[page - 1]

                cy.task('gerarTxt', { newRow: Object.values(newRow), page: 1, code: '0' })
                page++

            });

        }

        cy.wait(1000)
    });
});