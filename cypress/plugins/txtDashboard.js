const fs = require('fs')

const gerarTxt = ({ newRow, page, code }) => {
    fs.access('cypress/dashboard_txt/' + page + '_Dashboard_' + code + '.txt', fs.constants.R_OK, (err) => {
        console.log('\n> Checando existencia do arquivo')
        if (err) {
            let writeStream = fs.createWriteStream('cypress/dashboard_txt/' + page + '_Dashboard_' + code + '.txt')
            writeStream.write((newRow.toString()).replaceAll(',', ';'))
            writeStream.write('\r\n')
            writeStream.end()
            console.error('Arquivo criado')
        } else {
            console.log('Escrevendo no fim do arquivo')
            fs.appendFile('cypress/dashboard_txt/' + page + '_Dashboard_' + code + '.txt', newRow.toString().replaceAll(',', ';') + '\r\n', (err) => {
                if (err) throw err
                console.log('Salvo!')
            })
        }
    })
    console.log('Project ID:' + newRow[8])
    console.log('Data: ' + newRow[0])
    console.log('Run: ' + newRow[1])
    console.log('Link: ' + newRow[2])
    console.log('Pulados: ' + newRow[3])
    console.log('Ignorados: ' + newRow[4])
    console.log('Aprovados: ' + newRow[5])
    console.log('Reprovados: ' + newRow[6])
    console.log('Total: ' + newRow[7])

    return 'Salvo'

}


module.exports = {
    gerarTxt,
}