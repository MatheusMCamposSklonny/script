@echo off
cls
@echo.
@echo ****************************************************
@echo ********  CARGA DASHBOARD TCC - PostgreSQL  ********
@echo ****************************************************
@echo.
SET currentdir=%~dp0
SET kitchen=%currentdir%\data-integration\Kitchen.bat
SET logfile="%currentdir%log.txt"
 
echo. >> %logfile%
echo. >> %logfile%
"%kitchen%" /file:"%currentdir%cargadashboard.kjb" /level:Basic >> %logfile%